const defaultState = {
    name: "ALH 太郎",
    tweet: {
      title: '',
      text: ''
    }
  }
  
  export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case 'UPDATE_TWEET':
            return {
                ...state,
                tweet: {
                    title: action.title,
                    text: action.text
                }
            }
        case 'UPDATE_NAME':
            return {
                ...state,
                name: action.name
            }

        default:
        return state;
    }
  }